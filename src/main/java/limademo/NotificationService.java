package limademo;

public interface NotificationService {

    void sendNotification(Person person, String message);
}
