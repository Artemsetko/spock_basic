package limademo

import spock.lang.Specification
import spock.util.mop.ConfineMetaClassChanges

class E10_ConfineMetaClassChanges extends Specification {

    @ConfineMetaClassChanges(String)
    void 'should have sayHi method on String'() {
        given:
            String.metaClass.sayHi = { -> "Hi ${delegate}" }

        expect:
            "Lima Team".sayHi() == 'Hi Lima Team'
    }

    void 'should not have sayHi method on String'() {
        when:
            "Lima Team".sayHi()

        then:
            thrown MissingMethodException
    }

}
