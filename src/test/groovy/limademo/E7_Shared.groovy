package limademo


import spock.lang.Shared
import spock.lang.Specification

class E7_Shared extends Specification {

    //@Shared
    def person = new Person('John', 'Doe')

    void 'should get name'() {
        given:
            println person

        expect:
            person.name == 'John'
    }

    void 'should get lastName'() {
        given:
            println person

        expect:
            person.lastName == 'Doe'
    }

}
