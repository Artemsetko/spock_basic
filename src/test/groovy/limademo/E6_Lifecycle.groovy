package limademo


import spock.lang.Specification

class E6_Lifecycle extends Specification {

    def person = new Person('John', 'Doe')

    def setupSpec() {
        println '>> setupSpec'
    }

    def setup() {
        println '>>> setup'
    }

    def cleanup() {
        println '>>> cleanup'
    }

    def cleanupSpec() {
        println '>> cleanSpec'
    }

    void 'should get name'() {
        expect:
            println 'should get name'
            person.name == 'John'
    }

    void 'should get lastName'() {
        expect:
            println 'should get lastName'
            person.lastName == 'Doe'
    }

}
