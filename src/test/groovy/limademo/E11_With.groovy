package limademo


import spock.lang.Specification

class E11_With extends Specification {

    void 'should check value on person'() {
        when:
            def person = new Person('John', 'Doe')

        then:
            with(person) {
                name == 'John'
                lastName == 'Doe'
            }

    }

}
